// Owais Naeem
// CSC 220: Intro to Algorithms
// Project #2: Huffman Compression/Decompression
// File: main.cpp

/*
		NOTE TO GRADER:		Only compression is completed. Options 1, 2, and 4 function 
							but option 3 does not function. See Project report
							for details on how this program can compress etc.
*/

#include "node.h"
#include <boost/dynamic_bitset.hpp>
#include <iostream>
#include <iomanip>
#include <string>
#include <queue>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <vector>
#include <ctime>
using namespace std;
using namespace naeem_1;

typedef boost::dynamic_bitset<unsigned char> Bitset;
Bitset& operator<<(Bitset& lhs, bool val) {lhs.push_back(val); return lhs;}

void delete_tree(node* root);																									// USED
void print_frequency_table_to_console(node *root);
void print_and_delete_tree(node* root);
void print_frequency_table_to_file(node *root, ofstream &outFile);
void reverse_bitset(Bitset A, Bitset B);																						// USED
void initialize_map(string &code, vector<string> &data_value, vector<string> &data_code, node *root);							// USED
void insert_into_bitset(string &code, Bitset &bitset);																			// USED
void encode(Bitset &bitset, vector<string> &data_value, vector<string> &data_code, ofstream &outFile, ifstream &inFile, string input_file_name, string output_file_name);		// USED
wstring ReadUTF16(const string & filename);
void decode(Bitset &bitset, vector<string> &data_value, vector<string> &data_code, ofstream &outFile, ifstream &inFile);
void A_to_B(const char* input);  //function for converting ASCII to Binary
void write_codes_to_file(Bitset bitset, node *root, ofstream& outFile);
void initialize_map(string &code, vector<string> &data_value, vector<string> &data_code, node *root); 
void insert_into_bitset(string &code, Bitset &bitset); 

void scan_and_read(ifstream &inFile, vector<string> &data, string input_file_name)
{
	//----------read file--------------
	inFile.open(input_file_name.c_str());
	string word;
	while(inFile.good())
	{
		word = inFile.peek();
		if(word == " ")				// if space
		{
			inFile.ignore(1, ' ');
			data.push_back(word);
		}
		else if(word == "\n")		// if newline
		{
			inFile.ignore(1, '\n');
			data.push_back(word);
		}
		else						// if any word
		{
			inFile >> word;
			data.push_back(word);
		}
	}
	inFile.close();
}

void tally_occurrences(vector<string> &data, vector<int> &data_frequency, vector<string> &data_value)
{
	sort(data.begin(), data.end());
	int data_size = data.size();
	int i = 0;
	while(i < data_size)
	{
		int word_count = 0;
		data_value.push_back(data[i]);
		int j = i;
		while(j < data_size && data[j] == data[i])
		{
			word_count++;
			j++;
		}
		data_frequency.push_back(word_count);
		i = j;
	}
}

void sort_characters_on_frequency(vector<int> &data_frequency, vector<string> &data_value)
{
	// insertion sort to sort data_frequency and data_value based on # of occurrences
	int num; string val; int i;
	for(int j = 2; j < data_frequency.size(); j++)
	{
		num = data_frequency[j]; val = data_value[j];
		// insert data_frequency[j] into the sorted sequency data_frequency[1 ... j - 1]
		i = j - 1;
		while (i > 0 && data_frequency[i] > num)
		{
			data_frequency[i + 1] = data_frequency[i]; data_value[i + 1] = data_value[i];
			i = i - 1;
		}
		data_frequency[i+1] = num; data_value[i + 1] = val;
	}
}

void build_priority_queue(priority_queue<node*, vector<node*>, compare_node> &pq, vector<int> &data_frequency, vector<string> &data_value)
{
	// Create nodes and push them into a priority queue with words with LESS frequency having MORE priority:
	for(int j = 0; j < data_value.size(); j++)
	{
		node *temp = new node;
		temp->word = data_value[j]; 
		temp->frequency = data_frequency[j];
		temp->left = 0;
		temp->right = 0;
		pq.push(temp);
	}
}

void build_huffman_tree(priority_queue<node*, vector<node*>, compare_node> &pq, node *root)
{
	while(pq.size() > 1)
	{
		node *temp = new node;
		node *left = pq.top();	pq.pop();
		node *right = pq.top();	pq.pop();
		temp->frequency = left->frequency + right->frequency;
		temp->left = left;	
		temp->right = right;
		pq.push(temp);
	}
	root = pq.top();
	pq.pop();
}

void output_huffman_tree_file_name(ofstream &outFile, vector<int> &data_frequency, vector<string> &data_value)
{
	for(int i = 0; i < data_value.size(); i++)
		outFile << data_value[i] << " " << data_frequency[i] << " ";
}

void initialize_map(string &code, vector<string> &data_value, vector<string> &data_code, node *root)
{
	if(root != 0) 
	{
		if(root->left==0 && root->right==0) 
		{	
			data_value.push_back(root->word);				// insert WORD
			data_code.push_back(code);						// insert CODE
		}
		else 
		{
			if(root->left != 0)
			{
				code += "0";
				initialize_map(code, data_value, data_code, root->left);
				code.pop_back();
			}
			if(root->right != 0)
			{
				code += "1";
				initialize_map(code, data_value, data_code, root->right);
				code.pop_back();
			}
		}
	}
}

void write_codes_to_file(Bitset bitset, node *root, ofstream& outFile)
{
	// Perform a traversal of the tree to obtain new code words (going left is 0, going right is 1)
	// code word is complete only when a leaf node is reached
	// NOTE: bitset is accumulated in reverse! Correct code is from  most_significnt ---> least_significant
	if(root != 0) 
	{
		if(root->left==0 && root->right==0) 
		{	
			// reverse order of bits
			Bitset reversed_bitset;
			for(int i = bitset.size()-1; i >= 0; i--)
			{
				if(bitset[i]) reversed_bitset << 1;
				else reversed_bitset << 0;
			}
			// write word to file
			outFile << root->word << " ";
			// write byte to file
			ostream_iterator<char> osit(outFile);
			boost::to_block_range(reversed_bitset, osit);
			reversed_bitset.clear();
			outFile << " ";
		}
		else 
		{
			if(root->left != 0)
			{
				bitset << 0;
				write_codes_to_file(bitset, root->left, outFile);
				bitset.resize(bitset.size()-1); // get rid of last bit
			}
			if(root->right != 0)
			{
				bitset << 1;
				write_codes_to_file(bitset, root->right, outFile);
				bitset.resize(bitset.size()-1); // get rid of last bit
			}
		}
	}
}

void insert_into_bitset(string &code, Bitset &bitset)
{
	// insert code into bitset in reversed order
	for(int i = 0; i < code.length(); i++)
	{
		if(code[i]=='1') bitset << 1;
		else bitset << 0;
	}
}

int main()
{
	// display user header
	cout << " -------------------------------------------------------\n";
	cout << " Welcome to Owais Naeem's AMAZING Huffman Compression/Decompression Program!\n";
	cout << " -------------------------------------------------------\n\n";

	// get user input
	char choice; string input_file_name; string output_file_name; string huffman_tree_file_name;

	cout <<	"				 *** NOTE TO USER *** " << "\n\n";
	cout << " This program successfully compresses any input file to an output file." << endl;
	cout << " To COMPRESS a document, do the following: \n\n";
	cout << " STEP 1: Pick option (1) to generate the Huffman frequencies of any input file \n        of your choice.\n";
	cout << " STEP 2: Pick option (2) and, when prompted, input the Huffman file created \n         ";
	cout << " in (1) so that it can be used to compress the input file.\n\n";
	cout << " * As decompression does not work, option (3) will NOT do anything! ";
	cout << "\n					 THANK YOU! \n\n\n";
	cout << " -------------------------------------------------------"; 

	cout << " \nPick one of the following options: " << "\n\n";
	cout << "	(1) Choose input file name and output Huffman tree file name" << endl;
	cout << "	(2) Choose input file name, output file name, and Huffman tree" << "\n" << "	    file name to encode the input file into the output file" << endl;
	cout << "	(3) Choose input file name, output file name, and Huffman tree" << "\n" << "	    file name to decode the input file into the output file" << endl;
	cout << "	(4) Exit the program" << "\n\n";
	cout << " Enter your choice (Type 1, 2, 3, or 4 and press enter): ";
	cin >> choice; int take;
	cout << "\n\n";
	while(choice == '1' || choice == '2')
	{
		if(choice == '1') // DONE
		{
			// User can input file name and output Huffman tree file name
			cout << " -------------------------------------------------------\n";
			cout << " Enter input file name (including extension): ";
			cin >> input_file_name;
			cout << " Enter output Huffman tree file name (including extension): ";
			cin >> huffman_tree_file_name;
			cout << " -------------------------------------------------------\n";

			// 1. scan and read input_file_name into vector
			vector<string> data;
			ifstream inFile; ofstream outFile;
			scan_and_read(inFile, data, input_file_name);

			// 2. tally ocurrence of all characters
			vector<int> data_frequency;	// data_frequency[i] is # of occurence of data_value[i]
			vector<string> data_value;
			tally_occurrences(data, data_frequency, data_value);

			// 3. sort characters based on # of occurences
			sort_characters_on_frequency(data_frequency, data_value);

			// 4. & 5. Build priority queue and build huffman tree		
			priority_queue<node*, vector<node*>, compare_node> pq;
			for(int j = 0; j < data_value.size(); j++)
			{
				node *temp = new node;
				temp->word = data_value[j]; 
				temp->frequency = data_frequency[j];
				temp->left = 0;
				temp->right = 0;
				pq.push(temp);
			}

			// Build a huffman tree:
			while(pq.size() > 1)
			{
				node *temp = new node;
				node *left = pq.top();	pq.pop();
				node *right = pq.top();	pq.pop();
				temp->frequency = left->frequency + right->frequency;
				temp->left = left;	
				temp->right = right;
				pq.push(temp);
			}
			node *root = pq.top();
			pq.pop();
		
			// 6. create map associating words to their huffman codes
	/*		data_value.clear();			// to hold the WORDS
			vector<string> data_code;	// to hold the CODES corresponding to the WORDS
			string code;				// to hold the temporary code;
			initialize_map(code, data_value, data_code, root);
			Bitset bitset;

			outFile.open(huffman_tree_file_name.c_str());
			write_codes_to_file(bitset, root, outFile); // To write codes to file
			outFile.close();
	*/		
			// 6. output huffman tree in huffman_tree_file_name
			outFile.open(huffman_tree_file_name.c_str());
			output_huffman_tree_file_name(outFile, data_frequency, data_value);
			outFile.close();

			// 7. delete tree
			delete_tree(root);
		
			cout << " Huffman Tree file has been generated to be used for COMPRESSION!\n";
			cout << "\n -------------------------------------------------------\n\n";

			cout << " \nPick one of the following options: " << "\n\n";
			cout << "	(1) Choose input file name and output Huffman tree file name" << endl;
			cout << "	(2) Choose input file name, output file name, and Huffman tree" << "\n" << "	    file name to encode the input file into the output file" << endl;
			cout << "	(3) Choose input file name, output file name, and Huffman tree" << "\n" << "	    file name to decode the input file into the output file" << endl;
			cout << "	(4) Exit the program" << "\n\n";
			cout << " Enter your choice (Type 1, 2, 3, or 4 and press enter): ";
			cin >> choice; int take;
			cout << "\n\n";

		}
		else if(choice == '2')
		{
			// User can choose input file name, output file name, and Huffman tree file name to encode the input file into the output file
			cout << " -------------------------------------------------------\n";
			cout << " Enter input file name (including extension): ";
			cin >> input_file_name;
			cout << " Enter output file name (including extension): ";
			cin >> output_file_name;
			cout << " Enter output Huffman tree file name (including extension): ";
			cin >> huffman_tree_file_name;
			cout << " -------------------------------------------------------\n";

			const clock_t begin_time = clock();	// begin time

			// Inititalize data_value and data_frequency using what is in the file huffman_tree_file_name
			vector<int> data_frequency;	// data_frequency[i] is # of occurence of data_value[i]
			vector<string> data_value;
			ifstream inFile; ofstream outFile;
			inFile.open(huffman_tree_file_name.c_str());
			string word; int frequency;
			while(inFile.good())
			{
				word = inFile.peek();
				if(word == " ")				// if space
				{
					data_value.push_back(word);
					inFile.ignore(1, ' ');					// discard space
					inFile.ignore();						// discard buffering space
					inFile >> frequency;					// get frequency
					data_frequency.push_back(frequency);
					inFile.ignore();						// discard buffering space
				}
				else if(word == "\n")		// if newline
				{
					data_value.push_back(word);
					inFile.ignore(1, '\n');					// discard newline
					inFile.ignore();						// discard buffernig space
					inFile >> frequency;					// get frequency
					data_frequency.push_back(frequency);
					inFile.ignore();						// discard buffering space
				}
				else						// if any word
				{
					inFile >> word;							// get word
					data_value.push_back(word);
					inFile.ignore(1);						// skip next character
					inFile >> frequency;
					data_frequency.push_back(frequency);
					inFile.ignore();						// skip next character
				}
			}
			inFile.close();

			// pop out extra entry for " " (space) in both vectors
			data_value.pop_back();
			data_frequency.pop_back();
		
			// 3. sort characters based on # of occurences
			sort_characters_on_frequency(data_frequency, data_value);

			// 4. & 5. Build priority queue and build huffman tree
			priority_queue<node*, vector<node*>, compare_node> pq;
			for(int j = 0; j < data_value.size(); j++)
			{
				node *temp = new node;
				temp->word = data_value[j]; 
				temp->frequency = data_frequency[j];
				temp->left = 0;
				temp->right = 0;
				pq.push(temp);
			}

			// 6. Build a huffman tree:
			while(pq.size() > 1)
			{
				node *temp = new node;
				node *left = pq.top();	pq.pop();
				node *right = pq.top();	pq.pop();
				temp->frequency = left->frequency + right->frequency;
				temp->left = left;	
				temp->right = right;
				pq.push(temp);
			}
			node *root = pq.top();
			pq.pop();

			// 6. Creat map associating words to their codes 
			data_value.clear();			// to hold the WORDS
			vector<string> data_code;	// to hold the CODES corresponding to the WORDS
			string code;				// to hold the temporary code;
			initialize_map(code, data_value, data_code, root);
			delete_tree(root);

			// *7. ENCODE the file using data_word and data_code
			Bitset bitset;
			encode(bitset, data_value, data_code, outFile, inFile, input_file_name, output_file_name); // encode "input.txt" into "compressed.txt

			cout << " COMPRESSION" << " of " << input_file_name << " to " << output_file_name << " took " << float(clock() - begin_time) / CLOCKS_PER_SEC * 1000  << "ms!" << "\n\n\n";
			cout << "\n -------------------------------------------------------\n\n";
			cout << " \nPick one of the following options: " << "\n\n";
			cout << "	(1) Choose input file name and output Huffman tree file name" << endl;
			cout << "	(2) Choose input file name, output file name, and Huffman tree" << "\n" << "	    file name to encode the input file into the output file" << endl;
			cout << "	(3) Choose input file name, output file name, and Huffman tree" << "\n" << "	    file name to decode the input file into the output file" << endl;
			cout << "	(4) Exit the program" << "\n\n";
			cout << " Enter your choice (Type 1, 2, 3, or 4 and press enter): ";
			cin >> choice; int take;
			cout << "\n\n";
		}
	}
	if(choice == '3')
	{
		// call function "decode" here, but it is incomplete...

		cout << " This part of the assignment is INCOMPLETE!" << endl;
		cout << "\n\n\n\n\n ----------END OF PROGRAM HAS BEEN REACHED----------" << endl;

		cin >> take;
		return 0;
	}
	else if(choice == '4')
	{
		cout << "\n\n\n\n\n ----------END OF PROGRAM HAS BEEN REACHED----------" << endl;
		cin >> take;
		return 0;
	}
	else
	{
		cout << " Invalid choice!" << endl;
		cout << "\n\n\n\n\n ----------END OF PROGRAM HAS BEEN REACHED----------" << endl;
		cin >> take;
		return 0;
	}
}



void delete_tree(node* root)
{
	if(root != 0) {
		if(root->left==0 && root->right==0) 
			delete root;
		else {
			delete_tree(root->left);
			delete_tree(root->right);	
			delete root;
		}
	}
}

void reverse_bitset(Bitset A, Bitset B)
{
	for(int i = A.size()-1; i >= 0; i--)
	{
		if(A[i]) B << 1;
		else B << 0;
	}
}
 
void print_frequency_table_to_console(node *root)
{
	cout << left;
	if(root != 0) {
		if(root->left==0 && root->right==0) 
			cout  << setw(20) << root->word << setw(10) << root->frequency << endl;
		else {
			print_frequency_table_to_console(root->left);
			print_frequency_table_to_console(root->right);	
		}
	}
}

void print_and_delete_tree(node* root)
{
	if(root != 0)						// if root exists
	{
		if(root->left==0 && root->right==0) // root is a child node
		{
			cout << setw(20) << root->word << setw(5) << root->frequency << endl;
			delete root;
		}
		else							       // root is NOT a child node
		{
			print_and_delete_tree(root->left);
			print_and_delete_tree(root->right);	
			delete root;
		}
	}
}

void print_frequency_table_to_file(node *root, ofstream &outFile) 
{
	outFile << left;
	if(root != 0) {
		if(root->left==0 && root->right==0)
			outFile << setw(30) << root->word << setw(10) << root->frequency << endl;
		else {
			print_frequency_table_to_file(root->left, outFile);
			print_frequency_table_to_file(root->right, outFile);	
		}
	}
}

void encode(Bitset &bitset, vector<string> &data_value, vector<string> &data_code, ofstream &outFile, ifstream &inFile, string input_file_name, string output_file_name)
{
	inFile.open(input_file_name.c_str(), ios::binary);
	string word; int index=0;
	while(!inFile.eof())
	{
		// next word, space, or newline is: 
		word = inFile.peek();

		// move ahead by one word, space, or newline in the file:
		if(word == " ")				// if space
			inFile.ignore(1, ' ');
		else if(word == "\n")		// if newline
			inFile.ignore(1, '\n');
		else						// if any word
			inFile >> word;
		
		// find the index at which word is located in data_value:
		while(word != data_value[index])
			index++;
		//cout << "word: " << data_value[index] << "   code: " << data_code[index] << endl;

		// store the assocated code in data_code into the bitset:
		insert_into_bitset(data_code[index], bitset);
		index = 0;
	}
	inFile.close();

	outFile.open(output_file_name.c_str(), ios::binary);
    ostream_iterator<char> osit(outFile);
	boost::to_block_range(bitset, osit);
	bitset.clear();
	outFile.close();
}

wstring ReadUTF16(const string & filename)
{
	ifstream file(filename.c_str(), std::ios::binary);
    stringstream ss;
    ss << file.rdbuf() << '\0';
    return wstring((wchar_t *)ss.str().c_str());
}

void decode(Bitset &bitset, vector<string> &data_value, vector<string> &data_code, ofstream &outFile, ifstream &inFile)
{
	
	// load all data from "compressed.txt" into a string			***MODIFY LATER BY IMPLEMENTING STRING BUFFERING***
	ostringstream stream;
	char* buffer = new char;
	inFile.open("compressed.txt", ios::binary);
	inFile.read(buffer, 1024);
	A_to_B(buffer);

	delete buffer;
/*
	string word(buffer);
	copy(word.begin(), word.end(), std::ostream_iterator<byte_type>(stream, " "));
	cout << stream.str() << endl;
	inFile.close();
*/	
	/*
	inFile.open("compressed.txt", ios::binary);
	outFile.open("output.txt");
	char buff[128];
	inFile.read(buff, 128);
	for(int i = 0; i < 128; i++)
		outFile << buff[i];
	outFile.close();
	inFile.close();



	*/
	//outFile.close();
/*	
	inFile.open("compressed.txt", ios::binary);
	string infile;
	while(!inFile.eof()) 
	{
		inFile >> infile;
		cout << infile;
	}
	// bitset(string(infile));
	
	inFile.close();
*/
/*
	inFile.open("compressed.txt");
	vector<unsigned char> v;
	while(!inFile.eof())
		inFile >> v;
	Bitset temp(in);
	// istream_iterator<char> isit(inFile);
	//boost::from_block_range(isit., isit);
	inFile.close();
	outFile.open("output.txt");
    ostream_iterator<char> osit(outFile);
	boost::to_block_range(temp, osit);
	temp.clear();
	outFile.close();
	// put all of string's contents into a bitset	
	
	// decode contents of bitset using data_code and write out decoded words to "output.txt" using data_value

*/
}

void A_to_B(const char* input)  //function for converting ASCII to Binary
{
	int ascii;           // used to store the ASCII number of a character
	int length = strlen(input);        //find the length of the user's input
	
	cout << " ";
	
	for(int x = 0; x < length; x++)          //repeat until user's input have all been read
	// x < length because the last character is "\0"
	{
		ascii = input[x];        //store a character in its ASCII number
		
		/* Refer to http://www.wikihow.com/Convert-from-Decimal-to-Binary for conversion method used 
		 * in this program*/		
		
		char* binary_reverse = new char [9];       //dynamic memory allocation
		char* binary = new char [9];
		
		int y = 0;    //counter used for arrays
		
		while(ascii != 1)    //continue until ascii == 1
		{
			if(ascii % 2 == 0)    //if ascii is divisible by 2
			{
				binary_reverse[y] = '0';   //then put a zero
			}
			else if(ascii % 2 == 1)    //if it isnt divisible by 2
			{
				binary_reverse[y] = '1';   //then put a 1
			}
			ascii /= 2;    //find the quotient of ascii / 2
			y++;    //add 1 to y for next loop
		}
		
		if(ascii == 1)    //when ascii is 1, we have to add 1 to the beginning
		{
			binary_reverse[y] = '1';
			y++;
		}
		
		if(y < 8)  //add zeros to the end of string if not 8 characters (1 byte)
		{
			for(; y < 8; y++)  //add until binary_reverse[7] (8th element)
			{
				binary_reverse[y] = '0';
			}
		}
	
		for(int z = 0; z < 8; z++)  //our array is reversed. put the numbers in the rigth order (last comes first)
		{
			binary[z] = binary_reverse[7 - z];
		}
		
		for(int i = 0; i < 9; i++)
			cout << binary[i];

		cout << binary;   //display the 8 digit binary number
		
		delete [] binary_reverse;     //free the memory created by dynamic mem. allocation
		delete [] binary;
	}
	
	cout << endl;
}
