// Owais Naeem
// CSC 220: Intro to Algorithms
// Project #2: Huffman Compression/Decompression
// File: node.h

#ifndef NAEEM_NODE_H
#define NAEEM_NODE_H
#include <string>
using namespace std;

namespace naeem_1
{
	struct node
	{
		string word;
		int frequency;
		node *left;
		node *right;
	};
	
	class compare_node
	{
	public:
		bool operator() (node* n1, node* n2)
		{
			return (n1->frequency) > (n2->frequency); // '>' implies sorted as min-heap
		}
	};
}
#endif